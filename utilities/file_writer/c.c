// this is a utility that will simply write one of the strings that is 
// supplied to its second argument, to a file whose filename is 
// given by the first argument.
//   written on 1202406241.222010 by dwrr from the alpine macbook machine.
//
//           usage: ./filewriter filename.txt hello
// 
//    this utiltiy is actually meant to be used from the text editor, to allow
//    the shell functionality with the text editor to do basic piping and 
//    writing output to files. this is a simple way to help do that from within
//    the editor without using the main shell at all,  eg, zsh or sh.

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <iso646.h>
#include <errno.h>
#include <fcntl.h>
#include <unistd.h>

int main(int argc, const char** argv) {
	if (argc < 3) return puts("usage: ./filewriter <filename> <string>");
	const size_t string_length = strlen(argv[2]);
	const char* string = argv[2];
	const char* filename = argv[1];
	
	printf("writing %lu bytes to file %s...\n", string_length, filename);
	int file = open(filename, 
		O_WRONLY | O_APPEND | O_CREAT | O_EXCL, 
		S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH
	);
	if (file < 0) { printf("filewriter: error: open: %s\n", strerror(errno)); exit(1); }
	puts("opened file successfully.");
	write(file, string, string_length);
	printf("successfully wrote %lu bytes.\n", string_length);
	close(file);
}



