/*

	this is a utility to move a file into the trash directory located
	on my computer, with the intent of using this program mostly
	always instead of using the "rm" unix command which deletes the
	file permanently. this utility basically just calls  "rename()",
	pretty much.

	written on 1202406241.225042 by dwrr 

	usage: 		./run file_to_remove.txt
*/

#include <unistd.h>
#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <iso646.h>
#include <errno.h>
#include <string.h>

static const char* trash_directory = "/home/dwrr/trash/";

int main(int argc, const char** argv) {
	if (argc < 2) return puts("usage: ./rmtrash <file_to_move_to_trash>");

	char destination[4096] = {0};
	strncpy(destination, trash_directory, sizeof destination);
	strncat(destination, argv[1], sizeof destination);

	int r = rename(argv[1], destination);
	if (r < 0) {
		printf("error: could not rmtrash file %s: %s\n", argv[1], strerror(errno));
		exit(1);
	} else {
		printf("successfully moved %s to %s\n", argv[1], destination);
		exit(0);
	}
}

