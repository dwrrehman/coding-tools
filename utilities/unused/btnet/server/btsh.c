// this is the shell / server utility that 
// will be running on the riscv machine. 
// responds to transmissions with a reply.

#define client_address 		"F0:2F:4B:14:DB:1D"
#define connect_command 	"bluetoothctl connect "
#define disconnect_command 	"bluetoothctl disconnect "
#define isconnected_command 	"bluetoothctl devices Connected"
#define connect_time 		2
#define zero_time 		2
#define one_time 		8
#define transmit_buffer_size 	16

#include <stdio.h>
#include <stdlib.h>
#include <iso646.h>
#include <string.h>
#include <unistd.h>

static int is_connected(void) {
	FILE* f = popen(isconnected_command, "r");
	if (not f) { perror("popen"); abort(); }
	size_t length = 0;
	char line[512] = {0};
	while (fgets(line, sizeof line, f)) length += strlen(line);
	pclose(f);
	if (length > 2) return 1; return 0;
}

static void print_bytes(unsigned char* bytes, unsigned int count) {
	printf("printing %u bytes\n", count);
	for (unsigned int i = 0; i < count; i++) {
		if (i % 8 == 0) puts("");
		char str[64] = {(char) bytes[i]};
		if (bytes[i] < 32 or (unsigned char) bytes[i] >= 127)
			snprintf(str, sizeof str, "\033[7m#\033[0m");
		printf("%02hhx(%s) ", bytes[i], str);
	}
	puts("\n[end]");
}


int main(void) {

	printf("server: disconnecting to begin listening...");

	system(disconnect_command client_address);

	while (1) {

		puts("----------STAGE 1: recving data...----------");

		puts("waiting for start bit...");
		while (not is_connected()) {
			sleep(1);
		}

		unsigned char recv_bytes[transmit_buffer_size] = {0};
		unsigned int recv_byte_count = 0;



		for (unsigned int n = 0; n < transmit_buffer_size; n++) {

			printf("attempting to receive data for byte #%u...\n", n);

			unsigned int bits[64] = {0};
			unsigned int on = 1, count = 0;
			unsigned int bit_count = 0;

			while (bit_count < 8) {
				int c = is_connected();
				
				if (c) {
					//puts("is_connected = true");
					if (on) {
						//puts("ignoring connected state");
					} else {
						printf("pushed: count [ %u ] : (bitcount=%u)\n", 
							count, bit_count + 1
						);
						bits[bit_count++] = count;
						on = 1; count = 0;
					}
				} else {
					on = 0; count++;
					//printf("idle : count = %u\n", count);
				}
				sleep(1);
			}


			unsigned char result = 0;
			unsigned int final_bits[8] = {0};

			puts("printing bits: ");
			for (unsigned int i = 0; i < 8; i++) {
				printf("#%u:  %u ~ %u ~ %u\n", i, bits[i], bits[i + 8], bits[i + 16]);

				unsigned int a = bits[i] >= one_time - 1;
				unsigned int b = bits[i + 8] >= one_time - 1;
				unsigned int c = bits[i + 16] >= one_time - 1;

				if (a == b == c) final_bits[i] = a;
				if (a == b) final_bits[i] = a;
				if (b == c) final_bits[i] = b;
				if (a == c) final_bits[i] = c;
				
				result |= final_bits[i] << i;
			}

			printf("final_bits = {");
			for (unsigned int i = 0; i < 8; i++) {
				printf("%u ", final_bits[i]);
			}
			printf(" }   ==>  [ %02hhx ]  --->  %c   \n", result, (char) result);

			recv_bytes[recv_byte_count++] = (unsigned char) result;

			if (result == 0) break;


			puts("[done] ...moving to next byte...");
		}


		puts("\n\n");
		puts("DONE RECVING:  HERES THE RESULTS: ");
		print_bytes(recv_bytes, recv_byte_count);
		puts("\n\n");


		puts("----------STAGE 2: sending data...----------");


		unsigned char bytes[transmit_buffer_size] = {0};
		unsigned int byte_count = recv_byte_count + 1;

		bytes[byte_count++] = 'Y';
		for (unsigned int i = 0; i < recv_byte_count and byte_count < transmit_buffer_size; i++)
			bytes[byte_count++] = recv_bytes[i];


		puts("sending the following bytes as response: ");
		print_bytes(bytes, byte_count);

		for (unsigned int b = 0; b < byte_count; b++) {
			unsigned int byte = (unsigned int) bytes[b];

			printf("b%u: sending bits for %02x ...\n", b, byte);

			//for (unsigned int _ = 0; _ < 1; _++) {

				//printf("\tsending copy #%u of byte...\n", _);

				for (unsigned int i = 0; i < 8; i++) {
					unsigned int bit = (byte >> i) & 1;

					printf("\t\ti%u: sending bit %u...\n", i, bit);

					system(connect_command client_address);
					sleep(connect_time);
					system(disconnect_command client_address);

					//printf("\t\tsleeping for %us...\n", bit ? one_time : zero_time);
					//fflush(stdout);

					sleep(bit ? one_time : zero_time);
				}
			//}
		}
	
		puts("sending last closing bit...");
		system(connect_command client_address);
		sleep(connect_time);
		system(disconnect_command client_address);
		sleep(connect_time);
	}
}



















/*



	the general idea of the bluetooth connectionbit-based protocol is going to be: 

		

		bits are send over the network by    sending:


			1   :     connect, sleep(2), disconnect, sleep(10)

			0   :     connect, sleep(2), disconnect, sleep(4)




		using this, sending information looks like this:


			__|``|_____|``|_____________|``|_____|``|____|``|.
    
			        0             1           0       0

					this would send the hex constant   2   over the network.




		you always send a final connect to terminate the last bit.

			you send 4 bits at a time, receiving 3 copies of these bits, for redundancy

				and you do majority rules for those 3 bits, to determine the bit pattern. so yeah. 



			


		when you see a   0000 0000    byte   sent   then thats when the server responds with its own bit pattern, or several.


			the server keeps sending bytes until it sends a 0000 0000 in which it then goes back to listening again. 


				waiting for the next transmission from the client. 



			when the client recieves 0000 0000    it starts listening to the response from the server about the packet it just sent.


			the client listens by sampling the connection state, every 1 second,    and keeps track of the number of negative samples it reiceves (ie, isconnected_calls which return 0), after receiving the final positive samples after a series of positive samples. 

			the number of negative samples informs the receiver of whether it just received a zero or 1. it pushes this bit to an array

				which holds 12 bits at maximum.   when the number of bits that have been received is 12,  it passes it to majority_rule(12b) which returns a 4bit value, and then the system pushes that into an array of hex digits, which is the receive buffer. this is then turned into an array of bytes. 


		


						actually, i think i am going to send 24 bits at a time, so that we can always deal with bytes, never just 4 bits. yeah. probably. so yeah. 





			thats how receiving works 




	










*/














/*



			else if (not strcmp(command, "nl")) {
				if (data_count + 1 >= (int) sizeof data_buffer) {
					printf("error: data buffer is full (%u)\n", data_count);
				} else data_buffer[data_count++] = 10;
			}



issue these on the riscv machine, to send data:


	bluetoothctl connect F0:2F:4B:14:DB:1D

	bluetoothctl disconnect F0:2F:4B:14:DB:1D


issue this on the macbook once every second while in receiving-state  to recieve this data:

	blueutil --is-connected 6c-f6-da-5b-b0-fc 


---------------------------------------


conversely,   issue these on the macbook, to send data:


	blueutil --connect 6c-f6-da-5b-b0-fc 

	blueutil --disconnect 6c-f6-da-5b-b0-fc


issue this on the riscv machine, to receive data:


	bluetoothctl devices Connected

















t = 4
is_connected = true
pushed: count [ 4 ] : (bitcount=24)
printing bits: 
#0:  3 ~ 3 ~ 3
#1:  8 ~ 8 ~ 8
#2:  4 ~ 4 ~ 4
#3:  4 ~ 4 ~ 4
#4:  3 ~ 4 ~ 4
#5:  8 ~ 11 ~ 8
#6:  8 ~ 9 ~ 8
#7:  6 ~ 3 ~ 4
[done] ...moving to next byte...
attempting to receive data for byte #1...
is_connected = true
ignoring connected state
is_connected = true





we got these timings when receiving bytes:


#0:  3 ~ 3 ~ 3    -->  0
#1:  8 ~ 8 ~ 8    -->  1
#2:  4 ~ 4 ~ 4    -->  0
#3:  4 ~ 4 ~ 4    -->  0
#4:  3 ~ 4 ~ 4    -->  0
#5:  8 ~ 11 ~ 8   -->  1
#6:  8 ~ 9 ~ 8    -->  1
#7:  6 ~ 3 ~ 4    -->  0




so we can see there is about 2 to 3 seconds of noise here, which is quite substantial lol. 

but yeah, i think if we make our 1 bits







more timings:



 
#0:  8 ~ 9 ~ 8       1 
#1:  3 ~ 3 ~ 3       0
#2:  4 ~ 3 ~ 3       0
#3:  4 ~ 3 ~ 3       0
#4:  6 ~ 4 ~ 4       0
#5:  9 ~ 9 ~ 11      1
#6:  11 ~ 11 ~ 9     1
#7:  4 ~ 4 ~ 4       0





*/


