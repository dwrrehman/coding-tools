// 1202406274.195624 dwrr   btbit   program
// allows my riscv machine to communicate over bluetooth
// with my macbook, using bluetoothctl, and blueutil only.
// uses the connection bit of information to send 
// and receive in a very slow manner based on timing. 
// error correction and half-duplex communication are
// required for this medium, due to its severe limitations.
//
//                        lol.
// 
//   a := riscv machine
//   b := macos laptop
//

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <iso646.h>

#define client_address 		"6c-f6-da-5b-b0-fc"
#define connect_command 	"blueutil --connect "
#define disconnect_command 	"blueutil --disconnect "
#define isconnected_command 	"blueutil --is-connected "
#define connect_time 		2
#define zero_time 		2
#define one_time 		8
#define transmit_buffer_size 	16

enum cli_states {
	command_state,
	literal_state,
	binary_state,
};

static void print_bytes(unsigned char* bytes, unsigned int count) {
	printf("printing %u bytes\n", count);
	for (unsigned int i = 0; i < count; i++) {
		if (i % 8 == 0) puts("");
		char str[64] = {(char) bytes[i]};
		if (bytes[i] < 32 or (unsigned char) bytes[i] >= 127)
			snprintf(str, sizeof str, "\033[7m#\033[0m");
		printf("%02hhx(%s) ", bytes[i], str);
	}
	puts("\n[end]");
}



static int is_connected(void) {
	FILE* f = popen(isconnected_command client_address, "r");
	if (not f) { perror("popen"); abort(); }
	char line[512] = {0};
	fgets(line, sizeof line, f);
	pclose(f);
	if (line[0] == '1') return 1; return 0;
}


static void transfer_data(unsigned char* bytes, unsigned int* given_count) {


	puts("----------STAGE 1: sending data...----------");

	printf("transfer_data: sending %u bytes...\n", *given_count);

	print_bytes(bytes, *given_count);
	
	unsigned int byte_count = *given_count;

	for (unsigned int b = 0; b < byte_count; b++) {
		unsigned int byte = (unsigned int) bytes[b];

		printf("b%u: sending bits for %02x ...\n", b, byte);

		//for (unsigned int _ = 0; _ < 3; _++) {

			// printf("\tsending copy #%u of byte...\n", _);

			for (unsigned int i = 0; i < 8; i++) {
				unsigned int bit = (byte >> i) & 1;

				printf("\t\ti%u: sending bit %u...\n", i, bit);

				system(connect_command client_address);
				sleep(connect_time);
				system(disconnect_command client_address);

				//printf("\t\tsleeping for %us...\n", bit ? one_time : zero_time);
				//fflush(stdout);

				sleep(bit ? one_time : zero_time);
			}
		//}
	}

	puts("sending last closing bit...");
	system(connect_command client_address);
	sleep(connect_time);
	system(disconnect_command client_address);
	sleep(connect_time);




	puts("----------STAGE 2: recving data...----------");

	puts("waiting for start bit...");
	while (not is_connected()) {
		sleep(1);
	}

	unsigned char recv_bytes[transmit_buffer_size] = {0};
	unsigned int recv_byte_count = 0;


	for (unsigned int n = 0; n < transmit_buffer_size; n++) {

		printf("attempting to receive data for byte #%u...\n", n);

		unsigned int bits[10] = {0};
		unsigned int on = 1, count = 0;
		unsigned int bit_count = 0;

		while (bit_count < 8) {
			int c = is_connected();
			
			if (c) {
				//puts("is_connected = true");
				if (on) {
					//puts("ignoring connected state");
				} else {
					printf("pushed: count [ %u ] : (bitcount=%u)\n", 
						count, bit_count + 1
					);
					bits[bit_count++] = count;
					on = 1; count = 0;
				}
			} else {
				on = 0; count++;
				//printf("idle : count = %u\n", count);
			}
			sleep(1);
		}

		unsigned char result = 0;
		unsigned int final_bits[8] = {0};

		puts("printing bits: ");
		for (unsigned int i = 0; i < 8; i++) {
			printf("#%u:  %u ~ %u ~ %u\n", i, bits[i], bits[i + 0], bits[i + 0]);

			unsigned int a = bits[i] >= one_time - 1;
			unsigned int b = bits[i + 0] >= one_time - 1;
			unsigned int c = bits[i + 0] >= one_time - 1;

			if (a == b == c) final_bits[i] = a;
			if (a == b) final_bits[i] = a;
			if (b == c) final_bits[i] = b;
			if (a == c) final_bits[i] = c;
			
			result |= final_bits[i] << i;
		}

		printf("final_bits = {");
		for (unsigned int i = 0; i < 8; i++) {
			printf("%u ", final_bits[i]);
		}
		printf(" }   ==>  [ %02hhx ]  --->  %c   \n", result, (char) result);

		recv_bytes[recv_byte_count++] = (unsigned char) result;

		if (result == 0) break;


		puts("[done] ...moving to next byte...");
	}

	puts("\n\n");
	puts("DONE RECVING:  HERES THE RESULTS: ");
	print_bytes(recv_bytes, recv_byte_count);
	puts("\n\n");
}



static void print_help(void) {
	puts("commands:");
	puts("  . exit");
	puts("  . help");
	puts("  . literal");
	puts("  . binary");
	puts("  . data");
	puts("  . transmit");
	puts("  . o");
	puts("[done]");
}

static unsigned int string_to_int(char* string) {
	unsigned int r = 0, s = 1;
	for (unsigned int i = 0; i < (unsigned int) strlen(string); i++) {
		if (string[i] == '0') s <<= 1;
		else if (string[i] == '1') { r += s; s <<= 1; }
		else break;
	}
	return r;
}

int main(void) {

	unsigned char data_buffer[transmit_buffer_size] = {0};
	unsigned int data_count = 0;
	char command[128] = {0};
	unsigned int cli_state = 0;

	while (1) {

		if (cli_state == command_state) printf(": "); 
		else if (cli_state == literal_state) printf(". ");
		else if (cli_state == binary_state) printf("# ");
		fflush(stdout);

		fgets(command, sizeof command, stdin);

		if (not strlen(command)) continue;
		if (command[strlen(command) - 1] == 10) 
			command[strlen(command) - 1] = 0;

		if (cli_state == command_state) {
			if (not strcmp(command, "exit")) break;
			else if (not strcmp(command, "help")) print_help();
			else if (not strcmp(command, "literal")) cli_state = literal_state;
			else if (not strcmp(command, "binary")) cli_state = binary_state;
			else if (not strcmp(command, "o")) {
				for (unsigned int i = 0; i < 100; i++) puts("");
				printf("\033[H\033[2J");
			}
			else if (not strcmp(command, "data")) print_bytes(data_buffer, data_count);
			else if (not strcmp(command, "transmit")) transfer_data(data_buffer, &data_count);
			else if (not strcmp(command, "empty")) data_count = 0;
			else printf("unknown command: %s\n", command);

		} else if (cli_state == literal_state) {
			if (data_count + (unsigned int) strlen(command) > (unsigned int) sizeof data_buffer) {
				printf("error: data buffer is full (%u / %u)\n", data_count, transmit_buffer_size);
			} else {
				memcpy(data_buffer + data_count, command, strlen(command));
				data_count += strlen(command);
			}
			cli_state = command_state;

		} else if (cli_state == binary_state) {
			if (data_count + 1 > (unsigned int) sizeof data_buffer) {
				printf("error: data buffer is full (%u / %u)\n", data_count, transmit_buffer_size);
			} else {
				unsigned int n = string_to_int(command);
				if (n >= 256) {
					printf("error: data byte value exceeds range for 8 bits: n = %u\n", n);
				} else {
					data_buffer[data_count++] = (unsigned char) n;
				}
			}
			cli_state = command_state;

		} else {
			puts("ended up in error state, going to command state.");
			cli_state = command_state;
		}
	}
}






























/*

do /usr/bin/man
fgets

*/














/*



			else if (not strcmp(command, "nl")) {
				if (data_count + 1 >= (int) sizeof data_buffer) {
					printf("error: data buffer is full (%u)\n", data_count);
				} else data_buffer[data_count++] = 10;
			}



issue these on the riscv machine, to send data:


	bluetoothctl connect F0:2F:4B:14:DB:1D

	bluetoothctl disconnect F0:2F:4B:14:DB:1D


issue this on the macbook once every second while in receiving-state  to recieve this data:

	blueutil --is-connected 6c-f6-da-5b-b0-fc 


---------------------------------------


conversely,   issue these on the macbook, to send data:


	blueutil --connect 6c-f6-da-5b-b0-fc 

	blueutil --disconnect 6c-f6-da-5b-b0-fc


issue this on the riscv machine, to receive data:


	bluetoothctl devices Connected


*/


