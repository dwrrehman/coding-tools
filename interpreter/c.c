// a simple argument-less interpreter!
// this is a c program that will be a simple interpreter to run code on any machine!
// written on 1202406182.021222 by dwrr
// more done on 1202407217.005111 
// got more progress on 1202501245.213334

#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdbool.h>
#include <stdint.h>
#include <iso646.h>
#include <ctype.h>
#include <math.h>
#include <time.h>

typedef uint64_t nat;

int main(int argc, const char** argv) {

	if (argc < 2) exit(puts("usage: ./interpret <source_file>"));

	const char* filename = argv[1];
	int file = open(filename, O_RDONLY);
	if (file < 0) { perror("open"); exit(1); }
	const nat text_length = (nat) lseek(file, 0, SEEK_END);
	lseek(file, 0, SEEK_SET);
	char* text = malloc(text_length);
	read(file, text, text_length);
	close(file);
	//printf("debug: read file %s of %llu bytes..\n", filename, text_length); 


	enum isa_instructions {
		eoi, 
		next, first, 
		incr, zero, 
		bincr, bzero_,
		cincr, czero, 
		less, equal, true_, 
		goto_, at, 
		print,
		isa_count
	};

	const char* ins_spelling[isa_count] = {
		"eoi", 
		"next", "first", 
		"incr", "zero", 
		"bincr", "bzero", 
		"cincr", "czero", 
		"less", "equal", "true", 
		"goto", "at", 
		"print", 
	};

	nat* ins = NULL;
	nat ins_count = 0;

	for (nat start = 0, count = 0, i = 0; i < text_length; i++) {
		if (not isspace(text[i])) {
			if (not count) start = i;
			count++;
			if (i == text_length - 1) goto process; continue;
		} else if (not count) continue;
	process:;
		char* word = strndup(text + start, count);
		count = 0; 
		//printf("found word = %s\n", word);
		nat op = 0;		
		for (;op < isa_count; op++) {
			if (not strcmp(word, ins_spelling[op])) goto found;
		}
		//printf("error: unknown word \"%s\"\n", word); 
		//abort();
		continue;
	found:	if (op == eoi) break;
		ins = realloc(ins, sizeof(nat) * (ins_count + 1));
		ins[ins_count++] = op;
	}

	nat
		array[65536] = {0},
		labels[128] = {0},
		pc = 0, 
		flag = 0, 
		pointer = 0, 
		comparator = 0, 
		this = 0
	;
	
	while (pc < ins_count) {
		if (ins[pc] == bincr) this++;
		if (ins[pc] == bzero_) this = 0;
		if (ins[pc] == at) labels[this] = pc;
		pc++;
	}

	pc = 0;	
	while (pc < ins_count) {
		const nat op = ins[pc];
		if (op == next) pointer++;
		if (op == first) pointer = 0;
		if (op == cincr) comparator++;
		if (op == czero) comparator = 0;
		if (op == bincr) this++;
		if (op == bzero_) this = 0;
		if (op == incr) array[pointer]++;
		if (op == zero) array[pointer] = 0;
		if (op == less)  flag = comparator < array[pointer]; 
		if (op == equal) flag = comparator == array[pointer];
		if (op == true_) flag = 1;
		if (op == at) labels[this] = pc;
		if (op == goto_ and flag) pc = labels[this];
		if (op == print) 
			printf("%llu : 0x%016llx : @0x%016llx : c0x%016llx\n", 
				array[pointer], array[pointer], pointer, comparator
			);

		pc++;
	}
}







































// this is a test to see if we can gitpush.
// this is the second test, to see if we can git push. this is going well so far i think. nice. 

/*

  useful editor commands for building and running 
  the interpreter from within the editor.

copyb insert ./build
copya do ,./interpret,file.s
exit
*/






// another test here!

// this is a test to see if i can push changes to git from this machine, the raspberry pi zero 2 w computer.




































// this is an interpreter to be able to 
// run code in any machine i am on!
// i am writing this on 1202407217.014752
// using the text editor! i love it alot lol.
// yay








		// printf("at #%llu: c = %d\n", i, text[i]);




























		/*printf("found word: "
			"start = %llu, count = %llu\n", 
			start, count
		);*/




















