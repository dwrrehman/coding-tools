# my .zshrc file - written by daniel w. r. rehman.
# updated on 202401077.232650
# updated on 202406241.200310 for alpine linux laptop.

autoload -U compinit
compinit
setopt autocd autopushd

HISTFILE="/home/dwrr/.zsh_history"
SAVEHIST=500
PROMPT='%1~: '

# shortcuts and useful commands:
alias o="clear"
alias b="./build"
alias sl="ls --color=none"
alias s="ls --color=none"
alias ls="ls --color=none"
alias rm="rm -i -v"
alias mv="mv -i -v"
alias cp="cp -i -v"



# alias rt="trash"
# alias this="open ."
alias dt="date \"+%y%m%d%u.%H%M%S\""
# alias dtp="dt | pbcopy"
alias home="cd /home/dwrr/root/"
# alias index="/Users/dwrr/root/documents/utilities/tbindex"
alias t="/home/dwrr/root/editor/edit"

# applications:
#alias sub="open -a Sublime\ Text"
#alias edit="open -a TextEdit"
#alias notes="open -a Notes"
#alias pages="open -a Pages"
#alias int="open -a Safari"
#alias chrome="open -a Google\ Chrome"
#alias messages="open -a Messages"
#alias photos="open -a Photos"
#alias email="open -a Mail"
#alias musescore="open -a MuseScore\ 3"
#alias prism="open -a Prism\ Launcher"
#alias obs="open -a OBS"
#alias aud="open -a Audacity"
#alias discord="open -a Discord"
#alias settings="open -a System\ Preferences"
#alias monitor="open -a Activity\ Monitor"

#special applications:
# alias multimc="/Applications/MultiMC.app/Contents/MacOS/MultiMC"
#alias youtube="open -a Safari http://youtube.com"

#bindkey "^[r" forward-word
#bindkey "^[l" backward-word

#bindkey "^[u" beginning-of-line
#bindkey "^[d" end-of-line




#export PATH="/opt/homebrew/opt/berkeley-db/bin:$PATH"


#export LDFLAGS="-L/opt/homebrew/opt/berkeley-db/lib"


#export CPPFLAGS="-I/opt/homebrew/opt/berkeley-db/include"













# ------------- dead garbage code: ---------------------



#export PICO_SDK_PATH=~/src/pico/pico-sdk

#If you need to have llvm first in your PATH, run:


# export PATH="/opt/homebrew/Cellar/llvm/15.0.6.reinstall/bin:$PATH"
# export LDFLAGS="-L/opt/homebrew/Cellar/llvm/15.0.6.reinstall/lib"
# export CPPFLAGS="-I/opt/homebrew/Cellar/llvm/15.0.6.reinstall/include"
# export CC="/opt/homebrew/Cellar/llvm/15.0.6.reinstall/bin/clang"

# # alias minecraft="/Applications/MultiMC.app/Contents/MacOS/MultiMC"

# alias minecraftmc="open -a /Applications/MultiMC.app"
# alias minecraftlauncher="/Applications/Minecraft.app/Contents/MacOS/launcher"

# LS_COLORS='no=00;37:fi=38;5;251:di=38;5;69:ln=04;36:pi=40;33:so=01;35:bd=40;33;01:'
# export LS_COLORS
# zstyle ':completion:*' list-colors ${(s.:.)LS_COLORS}

# Development stuff:

# export SDKROOT=$(xcrun --sdk macosx --show-sdk-path)
# export PATH="/usr/local/opt/llvm/bin:$PATH"
# export CPPFLAGS="-I/usr/local/opt/llvm/include -isysroot $SDKROOT -Wl,-syslibroot,$SDKROOT,"
# export LDFLAGS="-L/usr/local/opt/llvm/lib -Wl,-rpath,/usr/local/opt/llvm/lib,-syslibroot,$SDKROOT,"

# PROMPT='%F{green}%m%f:%F{green}%1~%f$ '

