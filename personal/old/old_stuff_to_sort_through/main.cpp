//
//  main.cpp
//  codejam_qual
//
//  Created by Daniel Rehman on 1904066.
//  Copyright © 2019 Daniel Rehman. All rights reserved.
//
#include <iostream>
const std::string filepath = "/Users/deniylreimn/Documents/code/cpp/sandboxes/codejam_qual/codejam_qual/file.txt";
#include <fstream>
#include <sstream>
#include <vector>
#include <utility>
#include <algorithm>
#include <cmath>
#include <climits>
#include <assert.h>
#include <stdlib.h>

struct grid {
    size_t size = 0;
    std::string path = "";
};

static void advance(char c, size_t &e, size_t &s) {
    if (c == 'E') e += 1; else s += 1;
}

struct point {
    size_t e = 0;
    size_t s = 0;
};

struct point our = {};
struct point her = {};

struct point save_our() {
    return our;
}

void revert_our(struct point save) {
    our = save;
}

struct point save_her() {
    return her;
}

void revert_her(struct point save) {
    her = save;
}


std::pair<bool, std::string> compute(std::string our_current_path, const struct grid grid, const size_t her_index) {

    if (our.e == grid.size - 1 && our.s == grid.size - 1) {
        return {true, our_current_path};
    } else if (our.e >= grid.size || our.s >= grid.size) {
        return {false, our_current_path};
    }

    auto saved = save_our();
    auto her_saved = save_her();

    if (her.e == our.e && her.s == our.s) { // if we are on the same point...
        if (grid.path[her_index] == 'E') {
            our_current_path += "S";
        } else {
            our_current_path += "E";
        }
    } else {
        if (our.e < our.s) {      // pick a random one.
            our_current_path += "E";
        } else {
            our_current_path += "S";
        }
    }

    advance(grid.path[her_index], her.e, her.s);
    advance(our_current_path.back(), our.e, our.s); // make the move.

    auto next = compute(our_current_path, grid, her_index + 1);

    if (next.first) {
        return {true, next.second};

    } else {

        revert_our(saved);
        revert_her(her_saved);

        std::string other_choice = "";

        if (her.e == our.e && her.s == our.s) {
            return {false, our_current_path};
        }

        if (our_current_path.back() == 'E') {
            other_choice = "S";
        } else {
            other_choice = "E";
        }

        our_current_path.pop_back();
        our_current_path += other_choice;
        advance(our_current_path.back(), our.e, our.s);
        advance(grid.path[her_index], her.e, her.s);

        return compute(our_current_path, grid, her_index + 1);
    }
}


bool has_same_number_of_e_and_s(std::string string) {
    int e_count = 0;
    for (auto c : string) {
        if (c == 'E') {
            e_count++;
        }
    }

    int s_count = 0;
    for (auto c : string) {
        if (c == 'S') {
            s_count++;
        }
    }
    return e_count == s_count;
}



int main() {

{

    std::ofstream out;
    out.open(filepath, std::ios_base::trunc);
    srand((unsigned) time(NULL));

    const auto out_T = 100;
    out << out_T << std::endl;

    for (int i = 0; i < out_T; i++) {
        const auto board_size = rand() % 10 + 2;
        out << board_size << std::endl;


        std::string path = "";
        while (!has_same_number_of_e_and_s(path) || !path.length()) {
            path = "";
            for (int j = 0; j < (board_size - 1) * 2; j++) {
                if (rand() % 2) {
                    path += "E";
                } else {
                    path += "S";
                }
            }
        }
        out << path << std::endl;
    }
}


    const bool use_stdin = false;
    std::ifstream file_stream {filepath};
    std::istream& file = use_stdin ? std::cin : file_stream;

    std::vector<struct grid> N = {};
    size_t T = 0; file >> T;

    for (size_t i = 0; i < T; i++) {
        size_t n = 0;
        std::string s = "";

        file >> n; file >> s;
        N.push_back({n, s});
    }

    for (size_t i = 0; i < T; i++) {

        our = {0,0};
        her = {0,0};
        const auto path = compute("", N[i], 0);

        if (true) { // !path.first
            std::cout << "board size = " << N[i].size << "\n";
            std::cout << "our path = " << path.second  << "\n";
            std::cout <<  "her path = " << N[i].path  << "\n";
        }

        assert(N[i].path.length() == path.second.length());
        assert(path.second.length() == 2 * (N[i].size - 1));
        assert(our.e == N[i].size - 1 && our.s == N[i].size - 1);

        std::cout << "Case #" << i + 1 << ": " << path.second << "\n";
    }
}
